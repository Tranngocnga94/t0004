<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
<link href="/assets/js/slick/fullpage.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body>
<div class="header_cf">
	<h1 class="pc-only"><a href="#"><img src="/assets/image/common/logo2.png" alt=""></a></h1>
	<h2>手作りメガネ教室</h2>
</div>
<div class="l-wapp">
	<div class="l-container">
		<div class="l-wapp1">
			<img class="pc-only" src="/assets/image/common/img-16.png" alt="" width="803" height="116">
			<img class="sp-only" src="/assets/image/common/img-16sp.png" alt="" width="271" height="58">
			<p>入力内容をご確認の上、この内容でよろしければ「送信する」ボタンを押してください</p>
		</div>
		<div class="l-flame10">
			<form action="" method="post" class="form" id="form">
				<div class="table">
					<dl>
						<dt>
							<div class="title">
								<h6>お名前</h6>
							</div>
						</dt>
						<dd><!--{$name|escape}--></dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>年齢</h6>
							</div>
						</dt>
						<dd>
							<!--{$age|escape}--><span>歳</span>
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>性別</h6>
							</div>
						</dt>
						<dd class="radio">
							<!--{$gender|escape}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>お電話番号</h6>
							</div>
						</dt>
						<dd>
							<!--{$phone|escape}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>メールアドレス</h6>
							</div>
						</dt>
						<dd>
							<!--{$mail|escape}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご予約店舗</h6>
							</div>
						</dt>
						<dd class="radio">
							<!--{$store|escape}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご希望日時</h6>
							</div>
						</dt>
						<dd><span class="u-margin">日本橋人形町店</span>
							<div class="date">
								<!--{$month|escape}-->
								<span>月</span>
							</div>
							<div class="date">
								<!--{$day|escape}-->
								<span>日</span>
							</div>
							<div class="date">
								<!--{$year|escape}-->
								<span>時</span>
							</div>
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご参加予定人数</h6>
							</div>
						</dt>
						<dd>
							<!--{$people|escape}--><span>人</span>
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>その他</h6>
							</div>
						</dt>
						<dd>
							<!--{$comment|escape}-->
						</dd>
					</dl>
				</div>
				<div class="c-btn_cf">
					<button name="__retry_input__" type="submit" class="submit_cf1">入力内容を確認する</button>
					<button name="__send__" type="submit" class="submit_cf2">上記の内容で送信する</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!--{php}--> include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); <!--{/php}-->