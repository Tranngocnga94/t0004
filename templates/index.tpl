<!--{php}--> $id="index";<!--{/php}-->
<!--{php}--> include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php');<!--{/php}-->

<div class="c-mainvisual">
	<h1 class="pc-only"><a href="#"><img src="/assets/image/common/logo.png" alt="" width="231" height="70"></a></h1>
	<div class="c-mainvisual__cont">
		<img class="pc-only" src="/assets/image/common/img-01.png" alt="" width="465" height="466">
		<img class="sp-only" src="/assets/image/common/img-01sp.png" alt="" width="200" height="200">
	</div>
	<div class="c-mainvisual__btn">
		<a href="#"><span>ご予約・お問い合わせはこちら</span></a>
	</div>
</div>
<div class="l-wapper">
	<div class="l-flame1">
		<div class="l-container">
			<h2>作れるのは、メガネだけではありません。</h2>
			<p><span>好きなフレーム⽣地、デザインを選んで、メガネを⼿作りする。 <br>世界に一つだけのメガネとともに、大切な思い出もでき上がります。</span></p>
			<p>オリジナルメガネとして愛⽤するのはもちろん、 <br>テンプルにメッセージを⼊れて、贈り物にするのもおすすめです。</p>
		</div>
		<!-- <div class="l-flame1__bg"></div> -->
	</div>
	<div class="l-flame2 section">
		<div class="l-flame2__bg"></div>
		<div class="l-container">
			<div class="c-title1">
				<h3>手作りのメリット</h3>
				<img class="pc-only" src="/assets/image/common/icon-01.png" alt="">
				<img class="sp-only" src="/assets/image/common/icon-01sp.png" alt="" width="40" height="13">
			</div>
		</div>
		<div class="glass">
			<div class="row1">
				<!-- <div class="l-container"> -->
					<div class="l-flame2__form1">
						<div class="l-flame2__form1__img">
							<img class="pc-only" src="/assets/image/common/img-02.png" alt="" width="500" height="386">
							<img class="sp-only" src="/assets/image/common/img-02sp.png" alt="" width="305" height="221">
						</div>
						<div class="l-flame2__form1__text">
							<div class="c-title2">
								<img class="sp-only" src="/assets/image/common/icon-02sp.png" alt="" width="84" height="15">
								<h3>世界に一つの <br class="pc-only">メガネ<img class="pc-only" src="/assets/image/common/icon-02.png" alt=""></h3>
							</div>
							<p>自ら手作りしたメガネは世界に一つのメガネです。 <br class="pc-only">しかも、自分の顔にピッタリ＆使い心地もバツグンのメガネです。 <br><span>メガネの専門家があなたに最適なフレームを素材選びから一緒にお探しします！</span></p>
						</div>
					</div>
				<!-- </div> -->
			</div>
			<div class="row2">
				<!-- <div class="l-container"> -->
					<div class="l-flame2__form2">
						<div class="l-flame2__form2__text">
							<div class="c-title2">
								<img class="sp-only" src="/assets/image/common/icon-03sp.png" alt="" width="88" height="15">
								<h3>一生の <br class="pc-only">思い出に<img class="pc-only" src="/assets/image/common/icon-03.png" alt=""></h3>
							</div>
							<p><span>「メガネを手作りする」 <br class="pc-only">こんな体験はなかなかできるものではありません。</span><br>メガネとともに、かけがえのない思い出も作ることができます。</p>
						</div>
						<div class="l-flame2__form2__img">
							<img class="pc-only" src="/assets/image/common/img-03.png" alt="" width="500" height="386">
							<img class="sp-only" src="/assets/image/common/img-03sp.png" alt="" width="305" height="221">
						</div>
					</div>
				<!-- </div> -->
			</div>
			<div class="row3">
				<!-- <div class="l-container"> -->
					<div class="l-flame2__form1">
						<div class="l-flame2__form1__img">
							<img class="pc-only" src="/assets/image/common/img-04.png" alt="" width="500" height="386">
							<img class="sp-only" src="/assets/image/common/img-04sp.png" alt="" width="305" height="220">
						</div>
						<div class="l-flame2__form1__text">
							<div class="c-title2">
								<img class="sp-only" src="/assets/image/common/icon-04sp.png" alt="" width="88" height="15">
								<h3>プレゼントや <br class="pc-only">贈り物に<img class="pc-only" src="/assets/image/common/icon-04.png" alt=""></h3>
							</div>
							<p>世界に一つのオリジナルメガネを贈れば忘れられないプレゼントになります。 <br><span>テンプルにオリジナルメッセージを入れることもできます。</span>パートナー、家族、友達同士への、記念日の贈り物におすすめです。</p>
						</div>
					</div>
				<!-- </div> -->
			</div>
			<div class="row4">
				<!-- <div class="l-container"> -->
					<div class="l-flame2__form2">
						<div class="l-flame2__form2__text">
							<div class="c-title2">
								<img class="sp-only" src="/assets/image/common/icon-05sp.png" alt="" width="89" height="15">
								<h3>毎日使うもの <br class="pc-only">だから<img class="pc-only" src="/assets/image/common/icon-05.png" alt=""></h3>
							</div>
							<p><span>自分に合った、自分だけのメガネを自らの手で作ることができます。</span><br>毎日使うものだからこそ、愛着が持てるメガネを作っていただきたいのです。</p>
						</div>
						<div class="l-flame2__form2__img">
							<img class="pc-only" src="/assets/image/common/img-05.png" alt="" width="500" height="386">
							<img class="sp-only" src="/assets/image/common/img-05sp.png" alt="" width="305" height="221">
						</div>
					</div>
				<!-- </div> -->
			</div>
		</div>
	</div>
	<div class="l-flame3">
		<div class="l-container">
			<div class="c-btn1">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-phone">
				<div class="c-phone__1">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					03-3661-2906</h4>
					<p>日本橋人形町店<br class="sp-only"><span>（10:00-19:00）</span></p>
				</div>
				<div class="c-phone__2">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					042-648-2311</h4>
					<p>八王子店<br class="sp-only"><span>（9:30-19:00）</span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame4 section">
		<div class="l-container">
			<div class="c-title5">
				<h3>完成までのステップ</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
		</div>
		<div class="l-flame4__01">
			<div class="l-flame4__01__row1">
				<div class="l-flame4__img sp-only">
					<img src="/assets/image/common/img-06sp.png" alt="" width="320" height="187">
				</div>
				<div class="l-container">
					<div class="l-flame4__info">
						<div class="c-title3">
							<p>STEP <span>01</span></p>
						</div>
						<div class="c-title4">
							<h3>フレーム選び</h3>
						</div>
						<p class="c-text1"><span>150種以上</span>の生地からフレーム素材を選択。 <br class="pc-only">フロント、テンプルそれぞれに、馴染みやすい色・柄を選ぶか、あえてまったく異なるタイプを選ぶか、個性の見せどころです！</p>
					</div>
				</div>
			</div>
			<div class="l-flame4__01__row2">
				<div class="l-flame4__img sp-only">
					<img src="/assets/image/common/img-07sp.png" alt="" width="320" height="187">
				</div>
				<div class="l-container">
					<div class="l-flame4__info">
						<div class="c-title3">
							<p>STEP <span>02</span></p>
						</div>
						<div class="c-title4">
							<h3>デザイン選び</h3>
						</div>
						<p class="c-text1"><span>50種以上</span>のパターンの中から選びます。<br>選択に迷う場合は、メガネのスペシャリストであるスタッフが、お顔立ちにお似合いのデザインをおすすめします。</p>
					</div>
				</div>
			</div>
			<div class="l-flame4__01__row3">
				<div class="l-flame4__img sp-only">
					<img src="/assets/image/common/img-08sp.png" alt="" width="320" height="187">
				</div>
				<div class="l-container">
					<div class="l-flame4__info">
						<div class="c-title3">
							<p>STEP <span>03</span></p>
						</div>
						<div class="c-title4">
							<h3>フレームの切り出し</h3>
						</div>
						<p class="c-text1">パターン紙を生地に貼り付け、糸のこぎりで切り出します。
							最初は少し難しさを感じるかもしれませんが、<span>コツをつかめば、スムーズに作業が進められるようになります。</span><br>スタッフもサポートいたしますので、ご安心ください。
						</p>
					</div>
				</div>
			</div>
			<div class="l-flame4__01__row4">
				<div class="l-flame4__img sp-only">
					<img src="/assets/image/common/img-09sp.png" alt="" width="320" height="187">
				</div>
				<div class="l-container">
					<div class="l-flame4__info">
						<div class="c-title3">
							<p>STEP <span>04</span></p>
						</div>
						<div class="c-title4">
							<h3>整形</h3>
						</div>
						<p class="c-text1">ヤスリとサンドペーパーを使って、切り出したフレームの形を整えます。</p>
					</div>
				</div>
			</div>
		</div>
		<div class="l-flame4__02">
			<div class="l-flame4__02__row1">
				<div class="l-flame4__img sp-only">
					<img src="/assets/image/common/img-10sp.png" alt="" width="320" height="187">
				</div>
				<div class="l-container">
					<div class="l-flame4__info l-flame4__info--left">
						<div class="c-title3">
							<p>STEP <span>05</span></p>
						</div>
						<div class="c-title4">
							<h3>最終仕上げ</h3>
						</div>
						<p class="c-text1">作業が完了したフレーム(写真の状態のフレーム)を当店より福井県<span>鯖江市のメガネ職人に送付</span>。職人の手により、完成させます。</p>
					</div>
				</div>
			</div>
			<div class="l-flame4__02__row2">
				<div class="l-container">
					<div class="l-flame4__info l-flame4__info--left">
						<div class="l-flame4__info__img sp-only">
							<img src="/assets/image/common/img-14sp.png" alt="" width="234" height="53">
						</div>
						<div class="c-title3">
							<p>STEP <span>06</span></p>
						</div>
						<div class="c-title4">
							<h3>完成 !</h3>
						</div>
						<p class="c-text1">作業完了後、約1ヶ月半で<span>フレーム完成</span>。 <br class="pc-only">当店にてお渡し・郵送のご要望に応じて、お届けいたします。</p>
						<div class="l-flame4__02__img">
							<img class="pc-only" src="/assets/image/common/img-11.png" alt="" width="638" height="342">
							<img class="sp-only" src="/assets/image/common/img-11sp.png" alt="" width="218" height="124">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame5">
		<a class="video" href="#">
			<img class="pc-only" src="/assets/image/common/title-03.png" alt="" width="360" height="91">
			<img class="sp-only" src="/assets/image/common/title-03sp.png" alt="" width="184" height="47">
			<p>完成までのステップ</p>
			<span class="action"></span>
		</a>
		<div class="cd-popup">
			<div class="cd-popup-container">
				<a class="cd-popup-close img-replace">CLOSE</a>
			</div>
		</div>
	</div>
	<div class="l-flame3">
		<div class="l-container">
			<div class="c-btn1">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-phone">
				<div class="c-phone__1">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					03-3661-2906</h4>
					<p>日本橋人形町店<br class="sp-only"><span>（10:00-19:00）</span></p>
				</div>
				<div class="c-phone__2">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					042-648-2311</h4>
					<p>八王子店<br class="sp-only"><span>（9:30-19:00）</span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame6 section">
		<div class="l-container">
			<div class="c-title5">
				<h3>体験レポート</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
		</div>
		<div class="slider">
			<div class="slider__row">
				<div class="slider__title">
					<p>CASE <span>01</span></p>
					<h4>結婚式での<br class="sp-only">サプライズプレゼントに！</h4>
				</div>
				<div class="slider__content">
					<div class="slider__text">
						<p>メガネを手作りできるとは思っていませんでした。 <br class="pc-only">'作るときも楽しくて。</p>
						<p>不器用なので、形になるのかと心配でしたが、出来上がりにビックリ！ <br class="pc-only">売っているメガネのようで、使えるメガネになっていて嬉しかったです。</p>
						<ul>
							<li>日本橋人形町店にて参加</li>
							<li>◯◯◯◯さん</li>
						</ul>
					</div>
					<div class="slider__img">
						<img src="/assets/image/common/img-15.png" alt="" width="400" height="240">
					</div>
				</div>
			</div>
			<div class="slider__row">
				<div class="slider__title">
					<p>CASE <span>01</span></p>
					<h4>結婚式での<br class="sp-only">サプライズプレゼントに！</h4>
				</div>
				<div class="slider__content">
					<div class="slider__text">
						<p>メガネを手作りできるとは思っていませんでした。 <br class="pc-only">'作るときも楽しくて。</p>
						<p>不器用なので、形になるのかと心配でしたが、出来上がりにビックリ！ <br class="pc-only">売っているメガネのようで、使えるメガネになっていて嬉しかったです。</p>
						<ul>
							<li>日本橋人形町店にて参加</li>
							<li>◯◯◯◯さん</li>
						</ul>
					</div>
					<div class="slider__img">
						<img src="/assets/image/common/img-15.png" alt="" width="400" height="240">
					</div>
				</div>
			</div>
			<div class="slider__row">
				<div class="slider__title">
					<p>CASE <span>01</span></p>
					<h4>結婚式での<br class="sp-only">サプライズプレゼントに！</h4>
				</div>
				<div class="slider__content">
					<div class="slider__text">
						<p>メガネを手作りできるとは思っていませんでした。 <br class="pc-only">'作るときも楽しくて。</p>
						<p>不器用なので、形になるのかと心配でしたが、出来上がりにビックリ！ <br class="pc-only">売っているメガネのようで、使えるメガネになっていて嬉しかったです。</p>
						<ul>
							<li>日本橋人形町店にて参加</li>
							<li>◯◯◯◯さん</li>
						</ul>
					</div>
					<div class="slider__img">
						<img src="/assets/image/common/img-15.png" alt="" width="400" height="240">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame7 section">
		<div class="l-container">
			<div class="c-title5">
				<h3>教室概要</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
			<div class="table">
				<dl>
					<dt>開催店舗</dt>
					<dd>東京メガネ  日本橋人形町店、八王子店 <br>※それぞれの店舗へのアクセスは <a href="#">こちら</a></dd>
				</dl>
				<dl>
					<dt>開催日</dt>
					<dd>日本橋人形町店：随時開催(日曜定休) <br>八王子店：毎週土日 <br>※事前予約が必要です。<a href="#">予約フォーム</a>予約フォーム</dd>
				</dl>
				<dl>
					<dt>時 間</dt>
					<dd>10：00～17：00（13：00～14：00は休憩時間）<br>※作業時間の目安は5～7時間。数日に分けて作業していただくことも可能です。</dd>
				</dl>
				<dl>
					<dt>定 員</dt>
					<dd>1日あたり2組（1組の人数：1～3名）</dd>
				</dl>
				<dl>
					<dt>費 用</dt>
					<dd>21,600円（税込）※レンズは別売りです。 ※文字入れは別途有料です（540円～）</dd>
				</dl>
				<dl>
					<dt>注意事項</dt>
					<dd>八王子店ではフレーム全体(フロント、テンプルとも)を手作りいただけます。 <br>日本橋人形町店ではフロントは手作り、テンプルは既製のものからお選びいただきます。</dd>
				</dl>
			</div>
		</div>
	</div>
	<div class="l-flame3">
		<div class="l-container">
			<div class="c-btn1">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-phone">
				<div class="c-phone__1">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					03-3661-2906</h4>
					<p>日本橋人形町店<br class="sp-only"><span>（10:00-19:00）</span></p>
				</div>
				<div class="c-phone__2">
					<h4><img class="pc-only" src="/assets/image/common/phone.png" alt="">
						<img class="sp-only" src="/assets/image/common/phone.png" alt="" width="13" height="17">
					042-648-2311</h4>
					<p>八王子店<br class="sp-only"><span>（9:30-19:00）</span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame8 section">
		<div class="l-container">
			<div class="c-title5">
				<h3>アクセス</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
		</div>
		<div class="l-flame8__map1">
			<div class="l-flame8__title sp-only">
					<h4>日本橋人形町店</h4>
				</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12962.808972895165!2d139.78371!3d35.684333!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018894ffa9b29c3%3A0x1766f288016a1e1e!2zSmFwYW4sIOOAkjEwMy0wMDEzIFTFjWt5xY0tdG8sIENoxavFjS1rdSwgTmlob25iYXNoaW5pbmd5xY1jaMWNLCAxIENob21l4oiSMTXiiJI2!5e0!3m2!1sen!2sus!4v1529292713264" allowfullscreen></iframe>
			<div class="l-flame8__info">
				<div class="l-flame8__title pc-only">
					<h4>日本橋人形町店</h4>
				</div>
				<div class="l-flame8__table">
					<dl>
						<dt><p>住所</p></dt>
						<dd>東京都中央区日本橋人形町1-15-6</dd>
					</dl>
					<dl>
						<dt><p>TEL</p></dt>
						<dd>03-3661-2906</dd>
					</dl>
					<dl>
						<dt><p>営業時間</p></dt>
						<dd>10：00～19：00（祝日は18：00閉店）</dd>
					</dl>
					<dl>
						<dt><p>定休日</p></dt>
						<dd>日曜日</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="l-flame8__map2">
			<div class="l-flame8__title sp-only">
					<h4>八王子店</h4>
				</div>
			<div class="l-flame8__info">
				<div class="l-flame8__title pc-only">
					<h4>八王子店</h4>
				</div>
				<div class="l-flame8__table">
					<dl>
						<dt><p>住所</p></dt>
						<dd>東京都八王子市旭町10-13 <br>カネダイビル5階</dd>
					</dl>
					<dl>
						<dt><p>TEL</p></dt>
						<dd>042-648-2311</dd>
					</dl>
					<dl>
						<dt><p>営業時間</p></dt>
						<dd>9：30～19：00</dd>
					</dl>
					<dl>
						<dt><p>定休日</p></dt>
						<dd>なし</dd>
					</dl>
				</div>
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.811063512455!2d139.33775631557!3d35.65702633890586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60191ddeb52ed1cb%3A0xf628165461105588!2sYamaguchi+Ophthalmology+Clinic!5e0!3m2!1sen!2s!4v1528777759881" allowfullscreen></iframe>
		</div>
	</div>
	<div class="l-flame9 section">
		<div class="l-container">
			<div class="c-title5">
				<h3>Q <span>&</span> A</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
			<div class="l-flame9__info">
				<div class="l-flame9__row">
					<p class="text1"><img class="pc-only" src="/assets/image/common/icon-13.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-13sp.png" alt="" width="26" height="14"> 参加したいけれど、所要時間5～7時間は少し長くて……</p>
					<p class="text1 text1--color"><img class="pc-only" src="/assets/image/common/icon-14.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-14sp.png" alt="" width="26" height="13">日を分割して作業していただくこともできます(その場合も費用は同じです)。ご予約の際にご相談ください。</p>
				</div>
				<div class="l-flame9__row">
					<p class="text1"><img class="pc-only" src="/assets/image/common/icon-15.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-15sp.png" alt="" width="26" height="14">フレームが完成したら、レンズも選べるの？</p>
					<p class="text1 text1--color"><img class="pc-only" src="/assets/image/common/icon-16.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-16sp.png" alt="" width="26" height="13"> レンズは別売りとなりますが、もちろん当店にてレンズをお買い求めいただけます。 <br>きめ細かい視力測定を経て、お客様の生活スタイルに最適なレンズを当店スタッフがご紹介いたします。</p>
				</div>
				<div class="l-flame9__row">
					<p class="text1"><img class="pc-only" src="/assets/image/common/icon-17.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-17sp.png" alt="" width="26" height="14">自分用ではなく、サプライズで家族へのプレゼントとして手作りしたいのですが……</p>
					<p class="text1 text1--color"><img class="pc-only" src="/assets/image/common/icon-18.png" alt="">
					<img class="sp-only" src="/assets/image/common/icon-18sp.png" alt="" width="26" height="13">お顔立ちに合わせたサイズのフレームを作成いたしますので、ご本人様におこしいただくのがベストですが、プレゼントされたいお相手がご愛用のフレームをお持ちになって、そちらを参考に手作りされたお客様もこれまでにいらっしゃいます。 <br>詳細は、当店スタッフまでお問合せ・ご相談くださいませ。</p>
				</div>
			</div>
		</div>
	</div>
	<div class="l-flame10 section">
		<div class="l-container">
			<div class="l-flame10__title">
				<h3>手作りメガネ教室 <br>予約申し込み・<br class="sp-only">お問い合わせフォーム</h3>
				<img src="/assets/image/common/icon-12.png" alt="">
			</div>
			<form action="" method="post" class="form" id="form">
				<!--{$hidden_param}-->
				<div class="table">
					<dl>
						<dt>
							<div class="title">
								<h6>お名前</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd><input type="text" name="name" placeholder="例）山田　太郎" value="<!--{$name|escape}-->"></dd>
					</dl>
					<dl class="null">
						<dt>
							<div class="title">
								<h6>年齢</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd><input class="u-width50" type="number" name="age" value="<!--{$age|escape}-->"><span>歳</span></dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>性別</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd class="radio">
							<!--{html_radios name="gender" options=$form_elements.gender assign=gender selected=$gender}-->
							<!--{$gender.0}-->
							<!--{$gender.1}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>お電話番号</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd>
							<input type="text" name="phone" placeholder="例）00-0000-0000" value="<!--{$phone|escape}-->">
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>メールアドレス</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd>
							<input type="text" name="mail" placeholder="例）sample@sample.com" value="<!--{$mail|escape}-->">
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご予約店舗</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd class="radio">
							<!--{html_radios name="store" options=$form_elements.store assign=store selected=$store}-->
							<!--{$store.0}--> <br class="sp-only">
							<!--{$store.1}-->
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご希望日時</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd><span class="u-margin">日本橋人形町店</span>
							<div class="date">
								<!--{html_options name="month" options=$form_elements.month assign=month selected=$month class="dateTime"}-->
								<span class="pc-only">月</span>
							</div>
							<div class="date">
								<!--{html_options name="day" options=$form_elements.day assign=day selected=$day class="dateTime"}-->
								<span class="pc-only">日</span>
							</div>
							<div class="date">
								<!--{html_options name="year" options=$form_elements.year assign=year selected=$year class="dateTime"}-->
								<span class="pc-only">時</span>
							</div>
						</dd>
					</dl>
					<dl>
						<dt>
							<div class="title">
								<h6>ご参加予定人数</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd>
							<input class="u-width50" type="number" name="people" value="<!--{$people|escape}-->"><span>人</span>
						</dd>
					</dl>
					<dl class="null">
						<dt>
							<div class="title">
								<h6>その他</h6>
								<p>必須</p>
							</div>
						</dt>
						<dd><textarea type="text" name="comment" placeholder="その他ご不明点、ご要望など、どんなことでもご記入ください。" value="<!--{$comment|escape}-->"></textarea></dd>
					</dl>
				</div>
				<div class="c-btn2">
					<button name="__submit__" type="submit" class="submit">入力内容を確認する</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--{php}--> include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); <!--{/php}-->