<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
<link href="/assets/js/slick/fullpage.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>

<div class="header_cf">
    <h1 class="pc-only"><a href="#"><img src="/assets/image/common/logo2.png" alt=""></a></h1>
    <h2>手作りメガネ教室</h2>
</div>
<div class="l-wapp">
    <div class="l-container">
        <div class="l-wapp2">
            <img class="pc-only" src="/assets/image/common/img-17.png" alt="" width="803" height="116">
            <img class="sp-only" src="/assets/image/common/img-17sp.png" alt="" width="271" height="58">
        </div>
        <div class="l-wapp__cont">
            <h6>お問い合わせが完了いたしました</h6>
            <p>お問い合わせありがとうございます。 <br>ご入力いただいたメールアドレス宛に後日ご連絡させていただきます。 <br>今しばらくお待ちくださいませ。</p>
            <p>数日経っても当社からの連絡が無い場合、大変お手数ですが、 <br>お電話またはメールフォームより再度お問い合わせください。</p>
        </div>
        <div class="btn-down">
            <a href="/">
                <span>東京メガネのホームページへ戻る</span>
            </a>
        </div>
    </div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>