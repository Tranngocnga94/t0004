<?php
function smarty_modifier_mb_encoding($string, $to="", $from="auto") {
	if ($to == "") {
		return false;
	}
	return mb_convert_encoding($string, $to, $from);
}