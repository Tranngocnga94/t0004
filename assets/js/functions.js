/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

// -------------------------------------------
// video popup
// -------------------------------------------
jQuery(document).ready(function($){
  //open popup
  $('.video').on('click', function(event){
    event.preventDefault();
    $(this).next('.cd-popup').addClass('is-visible');
    $('html, body').css({
      overflow: 'hidden',
    });
  });
  //close popup
  $('.cd-popup').on('click', function(event){
    if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
      event.preventDefault();
      $(this).removeClass('is-visible');
      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
    }
  });
});


// --------------------------------------------------
// slider
// --------------------------------------------------
$('.slider').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: '<img class="next" src="/assets/image/common/icon-11.png">',
    prevArrow: '<img class="pre" src="/assets/image/common/icon-10.png">',
    responsive: [
    {
      breakpoint: 768,
      settings: {
        centerMode: true,
        centerPadding: '25px',
        slidesToShow: 1,
        nextArrow: '<img class="next" src="/assets/image/common/icon-11sp.png">',
        prevArrow: '<img class="pre" src="/assets/image/common/icon-10sp.png">'
      }
    }
  ]
  });

// ------------------------------------------------------
// fullpage
// ------------------------------------------------------
if (screen.width > 767) {
  jQuery(document).ready(function() {
    jQuery('#pagetop').fullpage({
      navigation: true,
      navigationPosition: 'right',
      autoScrolling:false,
      fitToSection: false,
      scrollBar: true,
      navigationTooltips: ['手作りのメリット', '完成までのステップ', '体験レポート', '教室概要', 'アクセス', 'Q&A', '予約申し込み・お問い合わせ'],
      showActiveTooltip: true,
    });
  });
}

$(window).resize(function() {
  if ($(window).width() < 768) {
    location.reload();
  } else if ($(window).width() > 767) {
    location.reload();
  }
});

// ---------------------------------------------
// form
// ---------------------------------------------
// $(function(){
//   var errorTop = $("#form").offset().top;
//   $('html,body').animate({ scrollTop: errorTop }, 0);
//   return false;
// })

// ---------------------------------------------------
// select box
// ---------------------------------------------------

$(document).ready(function(){
  if ($(window).width() < 768) {
    // $('select[name="month"] option:nth-of-type(1)').html("jhjhj");
    $('select[name="month"] option:nth-of-type(1)').attr("label","月");
    $('select[name="day"] option:nth-of-type(1)').attr("label","日");
    $('select[name="year"] option:nth-of-type(1)').attr("label","時");
  }
  // if($('#fp-nav ul li:nth-of-type(1) a').hasClass('active')){
  //   $('.swiper li:nth-of-type(1)').addClass('is-active');
  // } else{
  //   $('.swiper li').removeClass('is-active');
  // }
});