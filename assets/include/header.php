<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
<link href="/assets/js/slick/fullpage.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

	<header id="header" class="sp-only">
		<h1><a href="#"><img src="/assets/image/common/logosp.png" alt="" width="84" height="25"></a></h1>
		<input id="burger" type="checkbox" />
		<label for="burger">
			<span></span>
			<span></span>
			<span></span>
		</label>
		<nav>
			<ul>
				<li><a href="#">手作りのメリット</a></li>
				<li><a href="#">完成までのステップ</a></li>
				<li><a href="#">体験レポート</a></li>
				<li><a href="#">体験レポート</a></li>
				<li><a href="#">アクセス</a></li>
				<li><a href="#">Q&A</a></li>
				<li><a href="#">予約申し込み・お問い合わせ</a></li>
			</ul>
		</nav>
	</header><!-- /header -->